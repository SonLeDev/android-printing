package com.example.sonle.mobapp;

import java.util.List;

/**
 * Created by sonle on 7/22/15.
 */
public class DataContent {
    private String soId;
    private String bienBanSo;
    private String thoiGian;
    private String viDo;
    private String kinhDo;
    private List<String> lanCan;
    private String tong;
    private String dvKiemTra;
    private String tenNguoiLai;
    private String bienSoXe;
    private String diaDiemKiemTra;
    private String nguoiLamChung;

    public String getSoId() {
        return soId;
    }

    public void setSoId(String soId) {
        this.soId = soId;
    }

    public String getBienBanSo() {
        return bienBanSo;
    }

    public void setBienBanSo(String bienBanSo) {
        this.bienBanSo = bienBanSo;
    }

    public String getThoiGian() {
        return thoiGian;
    }

    public void setThoiGian(String thoiGian) {
        this.thoiGian = thoiGian;
    }

    public String getViDo() {
        return viDo;
    }

    public void setViDo(String viDo) {
        this.viDo = viDo;
    }

    public String getKinhDo() {
        return kinhDo;
    }

    public void setKinhDo(String kinhDo) {
        this.kinhDo = kinhDo;
    }

    public List<String> getLanCan() {
        return lanCan;
    }

    public void setLanCan(List<String> lanCan) {
        this.lanCan = lanCan;
    }

    public String getTong() {
        return tong;
    }

    public void setTong(String tong) {
        this.tong = tong;
    }

    public String getDvKiemTra() {
        return dvKiemTra;
    }

    public void setDvKiemTra(String dvKiemTra) {
        this.dvKiemTra = dvKiemTra;
    }

    public String getTenNguoiLai() {
        return tenNguoiLai;
    }

    public void setTenNguoiLai(String tenNguoiLai) {
        this.tenNguoiLai = tenNguoiLai;
    }

    public String getBienSoXe() {
        return bienSoXe;
    }

    public void setBienSoXe(String bienSoXe) {
        this.bienSoXe = bienSoXe;
    }

    public String getDiaDiemKiemTra() {
        return diaDiemKiemTra;
    }

    public void setDiaDiemKiemTra(String diaDiemKiemTra) {
        this.diaDiemKiemTra = diaDiemKiemTra;
    }

    public String getNguoiLamChung() {
        return nguoiLamChung;
    }

    public void setNguoiLamChung(String nguoiLamChung) {
        this.nguoiLamChung = nguoiLamChung;
    }

    @Override
    public String toString() {
        StringBuilder temp = new StringBuilder();
        temp.append(this.getBienBanSo()+"-")
                .append(this.getBienSoXe() + "-")
                .append(this.getDiaDiemKiemTra() + "-")
                .append(this.getDvKiemTra() + "-")
                .append(this.getKinhDo() + "-")
                .append(this.getLanCan() + "-")
                .append(this.getNguoiLamChung()+"-")
        ;
        return temp.toString();
    }
}
