package com.example.sonle.mobapp;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MyPrinter extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_printer);

//        final DataContent data = getDataContent();

        final Button button = (Button) findViewById(R.id.printBtn);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PrintManager printManager = (PrintManager)getSystemService(Context.PRINT_SERVICE);

                // Set job name, which will be displayed in the print queue
                String jobName = getApplicationContext().getString(R.string.app_name) + " Document";

                // Start a print job, passing in a PrintDocumentAdapter implementation
                // to handle the generation of a print document
                printManager.print(jobName, new MyPrintDocumentAdapters(getApplicationContext()),
                        null); //
            }

        });

        final Button cleanBtn = (Button) findViewById(R.id.cleanBtn);
        cleanBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                doClean();
            }
        });
    }

    private DataContent getDataContent() {
        final DataContent data = new DataContent();
        data.setBienBanSo("1111111");
        data.setBienSoXe("1111122222");
        data.setDiaDiemKiemTra("HCM");
        return data;
    }

    private void doClean() {
        final EditText txtInput = (EditText)findViewById(R.id.txtInput);
        txtInput.setText("");
    }

    private void doPrint(DataContent data) {

        final EditText txtInput = (EditText)findViewById(R.id.txtInput);
        final TextView txt = (TextView)findViewById(R.id.textView);

//        txt.setText(data.toString());

        // Get a PrintManager instance
        PrintManager printManager = (PrintManager)getSystemService(Context.PRINT_SERVICE);

        // Set job name, which will be displayed in the print queue
        String jobName = getApplicationContext().getString(R.string.app_name) + " Document";

        // Start a print job, passing in a PrintDocumentAdapter implementation
        // to handle the generation of a print document
        printManager.print(jobName, new MyPrintDocumentAdapters(getApplicationContext()),
                null); //


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_printer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class MyPrintDocumentAdapters extends PrintDocumentAdapter
    {
        Context context;

        public MyPrintDocumentAdapters(Context context)
        {
            this.context = context;
        }

        @Override
        public void onLayout(PrintAttributes oldAttributes,
                             PrintAttributes newAttributes,
                             CancellationSignal cancellationSignal,
                             PrintDocumentAdapter.LayoutResultCallback callback,
                             Bundle metadata) {
        }


        @Override
        public void onWrite(final PageRange[] pageRanges,
                            final ParcelFileDescriptor destination,
                            final CancellationSignal cancellationSignal,
                            final WriteResultCallback callback) {
        }

    }

}
